(ns rat-web.events-test
   (:require [cljs.test :refer-macros [deftest testing is]]
             [alphabase.base58 :as base58]
             [ajax.core :as ajax]
             [rat-web.events :as events]
             [rat-web.db :as db]
             [rat-web.time]
             [cljs.pprint :refer [pprint]]
             [cljs-crypto-rand.core :as crypto]))

(deftest test-new-id
  (testing "genearte a new random uid")
  (with-redefs [crypto/rand-u8 #(base58/decode "GqenKSDVDSWMQ8zuPS4qKi")]
    (is (= (events/new-id) "GqenKSDVDSWMQ8zuPS4qKi"))))


; TODO: figure out how to test persistent-reg-event-db


(deftest test-initialize-db-handler
  (testing "handler for db initialization event")
  (is (= (events/initialize-db-handler "unused1" "unused2") db/default-db)))


(deftest test-load-or-generate-auth-token
  (testing "handler for initializing auth token")
  (with-redefs [events/new-id #(str "i-am-a-new-id")]
    (let [db {:persistent {:auth-token "already-here"}}]
      (is (= (events/load-or-generate-auth-token-handler {} "unused")
             {:persistent {:auth-token "i-am-a-new-id"}}))
      (is (= (events/load-or-generate-auth-token-handler db "unused")
             {:persistent {:auth-token "already-here"}})))))


(deftest test-set-active-panel-handler
  (is (= (events/set-active-panel-handler {} ["event" "the-panel" "token"])
         {:active-panel "the-panel"})))


(deftest test-get-payments-handler
  (let [db {:persistent {:auth-token "the-auth-token"}}
        output (events/get-payments-handler {:db db} "unused")]
    (is (= (events/get-payments-handler {:db db} "unused"))
        {:http-xhrio {:method :get
                      :uri "http://localhost/payment"
                      :format (ajax/json-request-format)
                      :response-format
                      (ajax/json-response-format {:keywords? true})
                      :headers {:x-auth-token "the-auth-token"}
                      :on-sucess [:process-payment-response]
                      :on-failure [:process-payment-response-failure]}
         :db (assoc db :loading? true)})))


(deftest test-process-payment-response-failure
  (is (= (events/process-payment-response-failure-handler {} ["unused" "result"])
      {})))


(deftest test-process-payment-response-handler
  (let [result {:payments "payments" :your-id "your-id"}]
   (is
     (= (events/process-payment-response-handler {} ["unused" result])
      {:payments "payments"
       :your-id "your-id"}))))


(deftest test-update-auth-token-handler
  (is (= (events/update-auth-token-handler {:db {}} ["unsed" "an-auth-token"])
         {:db {:auth-token "an-auth-token"}
          :dispatch [:update-auth-token-storage "an-auth-token"]})))


(deftest test-update-auth-token-storage-handler
  (is (= (events/update-auth-token-storage-handler {} ["unused" "an-auth-token"])
         {:persistent {:auth-token "an-auth-token"}})))


(deftest test-get-contact-info-handler
  (let [db {:persistent {:x-auth-token "an-auth-token"}}]
    (is (= (events/get-contact-info-handler {:db {}} "unused"))
        {:http-xhrio {:method :get
                      :uri "http://localhost/contact-info"
                      :format (ajax/json-request-format)
                      :response-format (ajax/json-response-format
                                         {:keywords? true})
                      :headers {:x-auth-token "an-auth-token"}
                      :on-success [:process-contact-info-response]
                      :on-failure [:process-contact-info-response-failure]}
         :db (assoc db :loading true)})))


(deftest test-get-contact-info-response-handler
  (let [result {:contact-info "contacts"}]
    (is (= (events/process-contact-info-response-handler {} ["unused" result])
           {:contacts "contacts" :payment-contacts nil}))))


(deftest test-process-contact-info-response-failure-handler
  (is (= (events/process-contact-info-response-failure-handler
           {:contacts "contacts"}
           ["unused" "unused"]))
      {}))


(deftest test-send-payment-handler-known-recipient
  (with-redefs [events/new-id #(str "i-am-a-new-id")
                rat-web.time/timestamp-now #(str "a-timestamp")]
    (let [db {:persistent {:auth-token "an-auth-token"}}
          output (events/send-payment-handler {:db db} ["event" {:person "a-person"} "60"])]

      (is (= (get-in output [:http-xhrio :headers :x-auth-token]) "an-auth-token"))
      (is (= (get-in output [:http-xhrio :params :value] 3600)))
      (is (= (get-in output [:http-xhrio :params :timestamp]) "a-timestamp"))
      (is (= (get-in output [:http-xhrio :params :token]) "i-am-a-new-id"))
      (is (= (get-in output [:http-xhrio :params :recipient]) "a-person"))
      (is (nil? (get-in output [:http-xhrio :params :contact])))
      (is (= (second (get-in output [:http-xhrio :on-success])) "i-am-a-new-id")))))


(deftest test-send-payment-handler-unknown-recipient
  (with-redefs [events/new-id #(str "i-am-a-new-id")
                rat-web.time/timestamp-now #(str "a-timestamp")]
    (let [db {:persistent {:auth-token "an-auth-token"}}
          output (events/send-payment-handler {:db db} ["event" {:id "a-contact"} "60"])]
      (is (= (get-in output [:http-xhrio :params :contact]) "a-contact"))
      (is (nil? (get-in output [:http-xhrio :params :recipient]))))))


(deftest test-send-payment-response-handler
  (is (= (events/get-payment-response-handler {:db {}} ["event" "token" {:ok true}])
         {:dispatch [:get-payments] :db {:emailable-token "token"
                                         :payment-request-ok true
                                         :error-processing-payment nil
                                         :send-payment-loading? false}})))


(deftest test-send-payment-response-failure-handler
  (is (= (events/process-send-payment-response-failure-handler {} [event result])
         {:payment-request-ok false :send-payment-loading? false})))


(deftest test-contact-select-handler
  (is (= (events/contact-selection-handler {} ["event" "chosen_contact"])
         {:recipient-is-new false}))
  (is (= (events/contact-selection-handler {} ["event" "__new_contact__"])
         {:recipient-is-new true})))


(deftest test-add-contact-handler
  (with-redefs [events/new-id #(str "i-am-a-new-id")]
    (let [db {:db {:persistent {:auth-token "auth-token-xyz"}}}
          output (events/add-contact-handler db ["event" "description"])]
      (is (= (get-in output [:http-xhrio :params :id] "i-am-a-new-id")))
      (is (= (get-in output [:http-xhrio :headers :x-auth-token]) "auth-token-xyz")))))


(deftest test-process-add-contact-response
  (is (= (events/process-add-contact-response-handler {:db {}} ["event" "a-contact"])
         {:dispatch [:get-contact-info] :db {:contact-to-pay "a-contact"
                                             :payment-step :choose-value
                                             :add-contact-loading? false}})))


(deftest test-add-contact-response-failure-handler
  (is (= (events/process-add-contact-response-failure-handler {} ["event" "result"])
         {:contact-added-ok false :add-contact-loading? false})))


(deftest test-claim-payment
  (let [db {:persistent {:auth-token "an-auth-token"}}
        output (events/claim-payment-handler {:db {}} ["event" "claim-token"])]
    (is (= (get-in output [:http-xhrio :headers :x-auth-token] "an-auth-token")))
    (is (= (get-in output [:http-xhrio :uri] "/api/claim?token=claim-token")))))


(deftest test-process-claim-response-handler
  (is (= (events/process-claim-response-handler {:db {}} "args")
         {:dispatch [:get-payments]
          :db {:token-claimed-ok true :claim-payment-loading? false}})))


(deftest test-process-claim-payment-response-failure-handler
  (is (= (events/process-claim-payment-response-failure-handler {} ["event" "result"])
         {:token-claimed-ok false :claim-payment-loading? false})))


(deftest test-prefilled-claim-token-hander
  (is (= (events/prefilled-claim-token-handler {} ["event" "a-token"])
         {:prefilled-claim-token "a-token"})))
