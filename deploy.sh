for STATIC_FILE in resources/public/js/compiled/app.js \
    resources/public/index.html \
    resources/public/google-fonts/DvlFBScY1r-FMtZSYIYoYw.ttf \
    resources/public/google-fonts/HkF_qI1x_noxlxhrhMQYEKCWcynf_cDxXwCLxiixG1c.ttf \
    resources/public/google-fonts/LqowQDslGv4DmUBAfWa2Vw.ttf \
    resources/public/google-fonts/v0SdcGFAl2aezM9Vq_aFTQ.ttf \
    "resources/public/semantic-2.2.12.min.css"; do
    gzip -9 -k $STATIC_FILE
done


rsync --delete -ax resources/public/ commontime@www.commontime.trade:/var/www/html/ui

find resources -name \*.gz -exec rm '{}' \;
