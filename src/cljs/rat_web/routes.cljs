(ns rat-web.routes
  (:require-macros [secretary.core :refer [defroute]])
  (:import goog.History)
  (:require [secretary.core :as secretary]
            [goog.events :as gevents]
            [goog.history.EventType :as EventType]
            [re-frame.core :as re-frame]
            [rat-web.events :as events]))

(defn hook-browser-navigation! []
  (doto (History.)
    (gevents/listen
     EventType/NAVIGATE
     (fn [event]
       (secretary/dispatch! (.-token event))))
    (.setEnabled true)))

(defn app-routes []
  (secretary/set-config! :prefix "#")
  ;; --------------------
  ;; define routes here
  (defroute "/" []
    (re-frame/dispatch [::events/set-active-panel :landing-panel]))

  (defroute "/payment" []
    (re-frame/dispatch [::events/set-active-panel :payment-panel]))

  (defroute "/claim" []
    (re-frame/dispatch [::events/prefilled-claim-token nil])
    (re-frame/dispatch [::events/set-active-panel :claim-panel]))

  (defroute "/claim/:token" [token]
    (re-frame/dispatch [::events/prefilled-claim-token token])
    (re-frame/dispatch [::events/set-active-panel :claim-panel]))

  (defroute "/history" []
    (re-frame/dispatch [::events/set-active-panel :history-panel]))

  (defroute "/settings" []
    (re-frame/dispatch [::events/set-active-panel :settings-panel]))

  ;; --------------------
  (hook-browser-navigation!))
