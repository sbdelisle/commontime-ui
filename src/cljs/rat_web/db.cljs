(ns rat-web.db)


(def default-db
  {:name "Rat Web"
   :payments []
   :contacts []
   :payment-contacts []
   :profiles []
   :recipient-is-new true
   :payment-step :choose-contact
   :error-processing-payment nil
   :auth-token "a-good-token"})
