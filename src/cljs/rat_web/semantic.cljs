(ns rat-web.semantic
  (:require 
    [cljsjs.react.dom]
    [cljsjs.semantic-ui-react]
    [goog.object]))

; https://www.opensourcery.co.za/2017/02/12/using-semantic-ui-react-with-re-frame/
(def semantic-ui js/semanticUIReact)


(defn component
  "Get a component from sematic-ui-react:
    (component \"Button\")
    (component \"Menu\" \"Item\")"
  [k & ks]
  (if (seq ks)
    (apply goog.object/getValueByKeys semantic-ui k ks)
    (goog.object/get semantic-ui k)))


(def container (component "Container"))
(def menu (component "Menu"))
(def menu-item (component "Menu" "Item"))

(def divider (component "Divider"))
(def segment (component "Segment"))
(def segment-group (component "Segment" "Group"))

(def form (component "Form"))
(def form-input (component "Form" "Input"))
(def form-button (component "Form" "Button"))
(def form-group (component "Form" "Group"))

(def button (component "Button"))
(def label (component "Label"))

(def list-container (component "List"))
(def list-item (component "List" "Item"))

(def table (component "Table"))
(def table-row (component "Table" "Row"))
(def table-body (component "Table" "Body"))
(def table-header (component "Table" "Header"))
(def table-header-cell (component "Table" "HeaderCell"))

(def grid (component "Grid"))
(def grid-column (component "Grid" "Column"))
(def grid-row (component "Grid" "Row"))

(def table-cell (component "Table" "Cell"))

(def message (component "Message"))

(def transition-group (component "Transition" "Group"))

(def statistic (component "Statistic"))
(def statistic-label (component "Statistic" "Label"))
(def statistic-value (component "Statistic" "Value"))

(def icon (component "Icon"))
(def header (component "Header"))
(def header-content (component "Header" "Content"))
(def sub-header (component "Header" "Subheader"))

(def sticky (component "Sticky"))
