(ns rat-web.core
  (:require [reagent.core :as reagent]
            [re-frame.core :as re-frame]
            [rat-web.events :as events]
            [rat-web.routes :as routes]
            [rat-web.views :as views]
            [rat-web.config :as config]))


(defn dev-setup []
  (when config/debug?
    (enable-console-print!)
    (println "dev mode")))

(defn mount-root []
  (re-frame/clear-subscription-cache!)
  (reagent/render [views/main-panel]
                  (.getElementById js/document "app")))

(defn ^:export init []
  (routes/app-routes)
  (re-frame/dispatch-sync [::events/initialize-db])
  (re-frame/dispatch-sync [:load-or-generate-auth-token])
  (re-frame/dispatch [:get-payments])
  (re-frame/dispatch [:get-contact-info])
  (enable-console-print!)
  (dev-setup)
  (mount-root))
