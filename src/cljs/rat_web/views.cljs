(ns rat-web.views
  (:require [re-frame.core :as re-frame]
            [rat-web.demurrage :as dem]
            [rat-web.time :as rtime]
            [cljs-time.core :refer [now]]
            [goog.string :as gstring]
            [goog.dom :as gdom]
            [cemerick.url :refer (url url-encode)]
            [rat-web.semantic :refer [container
                                      menu
                                      menu-item
                                      statistic
                                      statistic-label
                                      statistic-value
                                      list-container
                                      list-item
                                      grid
                                      grid-column
                                      grid-row
                                      divider
                                      segment
                                      segment-group
                                      form
                                      form-input
                                      form-button
                                      form-group
                                      transition-group
                                      header
                                      sub-header
                                      header-content
                                      icon
                                      sticky
                                      button
                                      label
                                      message]]
            [rat-web.subs :as subs]))


(defn display-as-minutes [s]
  (gstring/format "%d" (/ s 60)))


; 10 9.0 .01 / 10 9.9 / 1.0 .99
(defn display-as-jiffies [seconds]
  (let [jiffies (/ seconds 360)
        magnitude (.abs js/Math jiffies)
        format-str (cond
                     (>= magnitude 9.95) "%.0f"
                     (>= magnitude 0.995) "%.1f"
                     :else "%.2f")]
    (gstring/format format-str jiffies)))


(defn get-value-from-input [input-id]
  (.-value (.getElementById js/document input-id)))


(defn reset-input-value [input-id]
  (gdom/setProperties (.getElementById js/document input-id) (clj->js {:value ""})))


(defn filter-treasurer [contacts]
  (filter #(not= (:id %) "__TREASURER__") contacts))


(defn calculate-balance [payments your-id]
  (let [debits (filter #(= your-id (:sender %)) payments)
        credits (filter #(= your-id (:recipient %)) payments)
        dem-debits (map #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                        debits)
        dem-credits (map
                      #(dem/demurrage (rtime/parse (:timestamp %)) (:value %) "seconds")
                      credits)
        shared-now (now)]
  (-
   (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-credits)) shared-now)
   (dem/quantity-at (apply dem/sum (concat [(dem/demurrage 0)] dem-debits)) shared-now))))


(defn payment-contact-description [person-id contact-list payment]
  (let
    [contacts-by-person (into {} (map #(vector (:person %) %) contact-list))
     contacts-by-id (into {} (map #(vector (:id %) %) contact-list))
     this-contact (cond
                    (= person-id (:recipient payment)) (get contacts-by-person (:sender payment))
                    (not (nil? (:recipient payment))) (get contacts-by-person (:recipient payment))
                    (not (nil? (:contact payment))) (get contacts-by-id (:contact payment))
                    :else nil)]
    (:description this-contact)))


(defn payment-value [person-id payment]
  (if
    (not= person-id (:sender payment))
    (dem/quantity-at
      (dem/demurrage (rtime/parse (:timestamp payment)) (:value payment) "seconds") (now))
    (dem/quantity-at
      (dem/demurrage (rtime/parse (:timestamp payment)) (- (:value payment))) (now))))


(defn display-minutes [minutes]
  [:span [:strong minutes] [:span {:class "ctm-symbol" :title "Common Time Minutes"} " jiffies"]])


(def nav-buttons
  [:> grid-column {:width 12}
   [:> button
    {:size "big" :link true :href "/#/payment" :class "primary-nav" :labelPosition "left"
     :icon true}
    [:> icon {:name "arrow up"}] "Make a Payment"]

   [:> button
    {:size "big" :link true :href "/#/claim" :class "primary-nav" :labelPosition "left"
     :icon true}
    [:> icon {:name "arrow down"}]  "Claim a Payment"]

   [:> button
    {:size "big" :link true :href "/#/history" :class "primary-nav" :labelPosition "right"
     :icon true}
    [:> icon {:name "history icon"}]  "Payment History"]])


(defn balance-box []
  (let [payments (re-frame/subscribe [::subs/payments])
        your-id (re-frame/subscribe [::subs/your-id])
        ctm (display-as-jiffies (calculate-balance @payments @your-id))]
    [:> segment
     [:> grid {:padded true}
       [:> grid-column {:width 4}
        [:> statistic {:color (if (< ctm 0) "red" "green")}
         [:> statistic-value ctm]
         [:> statistic-label "Balance (jiffies)"]]]
       nav-buttons]]))
 

(defn display-token [payment contact-description]
  (if
    (nil? (:recipient payment))
    [:a
     {:href (str
              "mailto:"
              contact-description
              "?"
              "subject="
              (url-encode (str "Commontime payment of " (display-as-jiffies (:value payment))
                               " jiffies") )
              "&"
              "body="
              (url-encode
                (str "A CommonTime user has sent you a payment of "
                     (display-as-jiffies (:value payment))
                     " jiffies at "
                     (:timestamp payment)
                     ". "
                     "You can claim your payment by visiting the following url:"
                     "\n\n\t\t"
                     "https://www.commontime.trade/#/claim/"
                     (:token payment))))}
     [:span {:style {:font-family "monospace"}} (:token payment)]]
    ""))


(defn display-timestamp [iso-timestamp]
  (let [ts (rtime/parse iso-timestamp)]
    [:span
     {:title (str (rtime/display-timestamp ts :minute) " UTC")}
     (str (rtime/display-timestamp ts :date) " UTC")]))


(defn history-panel-payment-row [payment]
  (let [contacts (re-frame/subscribe [::subs/contacts])
        your-id (re-frame/subscribe [::subs/your-id])
        contact-description (payment-contact-description @your-id @contacts payment)
        value (payment-value @your-id payment)]
    ^{:key (:token payment)}
    [:> grid-row
     [:> grid-column (display-timestamp (:record_time payment))]
     [:> grid-column [:strong contact-description]]
     [:> grid-column
      [:span
       {:class (if (< value 0) "negative" "positive")}
       (display-as-jiffies value)
       " jiffies"]]
     [:> grid-column
      (let [token-markup (display-token payment contact-description)]
        (if (= "" token-markup) "Transaction complete" [:span "Unclaimed " token-markup]))]]))


(defn shared-content [& specific]
  (let [logo
        [:> menu {:inverted true :borderless true :fixed "top" :class "banner"}
          [:> menu-item [:> header {:inverted true :textAlign "left"}
                         [:> icon {:name "hand rock" :size "big"}]
                         [:> header-content
                           "CommonTime"
                           [:> sub-header "Time for the common good."]]]]]

        begin-content
        [:> container {:class "main"}
         (balance-box)]
        content (if (empty? specific) begin-content (conj begin-content
                                                          [:> segment {:padded true} 
                                                           (first specific)]))]
    [:div logo content]))


(defn history-panel []
  (let [payments (re-frame/subscribe [::subs/payments])]
    (shared-content
     [:> segment
        [:> grid {:stackable true :columns 4 :container true :divided "vertically"}
        (doall
          (for [payment (reverse (sort-by :record_time @payments))]
            (history-panel-payment-row payment)))]])))


(defn landing-panel []
  (shared-content))


(defn select-contact [contact]
  (re-frame/dispatch [:contact-selected contact]))


(defn start-add-contact []
  (re-frame/dispatch [:start-add-contact]))


(defn display-choose-contact []
  (let [contacts (re-frame/subscribe [::subs/contacts])]
    [:> container
     [:> segment {:padded true}
      [:h1 "Choose contact to pay:"]
       [:> list-container
        {:horizontal true}

        (for [contact (filter-treasurer @contacts)]
          ^{:key (:id contact)}
          [:> list-item
           [:> form-input
            {:type "radio"
             :control "input"
             :on-click #(select-contact contact)
             :name "contact_to_pay"
             :label (:description contact)}]])]

       [:> divider {:horizontal true} "Or"]

       [:> form-button {:on-click start-add-contact} "Add Contact"]]]))


(defn send-payment [contact value]
  (re-frame/dispatch [:send-payment contact value]))


(defn update-value-to-pay []
  (re-frame/dispatch [:update-value-to-pay (get-value-from-input "payment_value")]))


(defn done-with-payment []
  (re-frame/dispatch [:done-with-payment]))


(defn display-choose-value []
  (let [contact-to-pay (re-frame/subscribe [::subs/contact-to-pay])
        value-to-pay (re-frame/subscribe [::subs/value-to-pay])
        value-paid (re-frame/subscribe [::subs/value-to-pay])
        last-sent-payment (re-frame/subscribe [::subs/last-sent-payment])
        send-payment-loading? (re-frame/subscribe [::subs/send-payment-loading?])
        error-processing-payment (re-frame/subscribe [::subs/error-processing-payment])
        payment-request-ok (re-frame/subscribe [::subs/payment-request-ok])]
    (println @last-sent-payment)
    [:> container
     [:> segment "Paying " (:description @contact-to-pay)]
     [:> segment
       [:> form
        {:error (= @payment-request-ok false)
         :success (= @payment-request-ok true)
         :loading (= true @send-payment-loading?)
         }
        [:> form-input {:id "payment_value"
                        :label "Amount to Pay (jiffies):"
                        :on-change update-value-to-pay
                        :auto-complete "off"}]
        [:> form-group
          [:> form-button {:on-click #(send-payment @contact-to-pay @value-to-pay)}
           "Pay"]
          [:> form-button {:on-click done-with-payment}
           (if (not (nil? @payment-request-ok)) "Done" "Cancel")]]

        [:> message
         {:success true}
         "Your payment of " (display-minutes @value-paid) " to "
         [:strong (:description @contact-to-pay)] " has been recorded. "
         (if (nil? (:recipient @last-sent-payment))
           [:span
            "Email the recipient the following token to complete the payment: "
           (display-token @last-sent-payment (:description @contact-to-pay))]
           "")]

        [:> message {:error true} "Error processing payment: " @error-processing-payment]]]]))


(defn update-contact-to-add []
  (re-frame/dispatch [:update-contact-to-add (get-value-from-input "contact_to_add")]))


(defn add-contact [contact]
  (re-frame/dispatch [:add-contact contact]))


(defn display-add-contact []
  (let [contact-description (re-frame/subscribe [::subs/contact-description])
        contact-added-ok (re-frame/subscribe [::subs/contact-added-ok])
        add-contact-loading? (re-frame/subscribe [::subs/add-contact-loading?])]
    [:> container
     [:> form {:error (= false @contact-added-ok)
               :loading (= true @add-contact-loading?)
               :size "huge"}

      [:h3 [:label {:for "contact_to_add"} "Email Address of contact to add:"]]
      [:> form-input {:placeholder "example@example.com"
                      :on-change update-contact-to-add
                      :id "contact_to_add"}]
      [:> form-group
        [:> form-button
         {:on-click #(add-contact @contact-description) :size "huge" :secondary true}
         "Add Contact"]
       [:> form-button {:on-click done-with-payment :size "huge"} "Cancel"]]
      [:> message {:error true} "Failed to add contact"]]]))


(defn payment-panel []
  (shared-content
   (let [payment-step (re-frame/subscribe [::subs/payment-step])]
     (println "payment-step" @payment-step)
     ((case @payment-step
       :choose-contact display-choose-contact
       :add-contact display-add-contact
       :choose-value display-choose-value)))))


(defn settings-panel []
  (shared-content
   [:div "Authentication Token:"
    [:input {:id "auth_token_input" :type "text"}]
    [:input
     {:id "update_auth_token_button"
      :type "button"
      :value "update"
      :on-click
      #(re-frame/dispatch [:update-auth-token (get-value-from-input "auth_token_input")])}]]))


(defn update-claim-token []
  (re-frame/dispatch [:update-claim-token (get-value-from-input "claim_token")]))


(defn submit-claim [claim-token]
  (re-frame/dispatch [:claim-payment claim-token]))


(defn claim-panel []
  (let [prefilled-claim-token (re-frame/subscribe [::subs/prefilled-claim-token])
        claim-token (re-frame/subscribe [::subs/claim-token])
        claim-payment-loading? (re-frame/subscribe [::subs/claim-payment-loading?])
        token-claimed-ok (re-frame/subscribe [::subs/token-claimed-ok])]
    (shared-content
     [:> form {:success (= true @token-claimed-ok)
               :error (= false @token-claimed-ok)
               :loading (= true @claim-payment-loading?)
               :size "huge"}
      (if (not @prefilled-claim-token)
        [:div
         [:h3 [:label {:for "claim_token"} "Payment token:"]]
         [:> form-input
           {:id "claim_token"
            :on-change update-claim-token}]]
        [:span "Claiming payment with token " @prefilled-claim-token "."])

      [:> form-button
       {:on-click #(submit-claim (or @claim-token @prefilled-claim-token))
        :secondary true
        :size "huge"} "Claim Payment"]

      [:> message {:success true} "The payment has been credited to your account."]
      [:> message {:error true} "Error claiming payment."]])))


(defn- panels [panel-name]
  (case panel-name
    :landing-panel [landing-panel]
    :payment-panel [payment-panel]
    :claim-panel [claim-panel]
    :history-panel [history-panel]
    :settings-panel [settings-panel]
    [:div]))


(defn show-panel [panel-name]
  [panels panel-name])


(defn main-panel []
  (let [active-panel (re-frame/subscribe [::subs/active-panel])]
    [show-panel @active-panel]))
