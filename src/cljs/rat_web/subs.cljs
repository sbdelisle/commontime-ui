(ns rat-web.subs
  (:require [re-frame.core :as re-frame]
            [rat-web.time :as rtime]
            [cljs-time.core :refer [now]]
            [rat-web.demurrage :as dem]))


(re-frame/reg-sub
 ::name
 (fn [db]
   (:name db)))


(re-frame/reg-sub
  ::auth-token
  (fn [db]
    (get-in db [:persistent :auth-token])))


(re-frame/reg-sub
  ::contacts
  (fn [db]
    (:contacts db)))


(re-frame/reg-sub
  ::payment-contacts
  (fn [db]
    (:payment-contacts db)))


(re-frame/reg-sub
  ::payments
  (fn [db]
    (:payments db)))


(re-frame/reg-sub
 ::active-panel
 (fn [db _]
   (:active-panel db)))


(re-frame/reg-sub
  ::your-id
  (fn [db _]
    (:your-id db)))


(re-frame/reg-sub
  ::recipient-is-new
  (fn [db _]
    (:recipient-is-new db)))

(re-frame/reg-sub
  ::prefilled-claim-token
  (fn [db _]
    (:prefilled-claim-token db)))


(re-frame/reg-sub
  ::claim-token
  (fn [db _]
    (:claim-token db)))


(re-frame/reg-sub
  ::token-claimed-ok
  (fn [db _]
    (:token-claimed-ok db)))


(re-frame/reg-sub
  ::payment-step
  (fn [db _] (:payment-step db)))


(re-frame/reg-sub
  ::contact-to-pay
  (fn [db _] (:contact-to-pay db)))


(re-frame/reg-sub
  ::contact-description
  (fn [db _] (:contact-description db)))


(re-frame/reg-sub
  ::contact-added-ok
  (fn [db _] (:contact-added-ok db)))


(re-frame/reg-sub
  ::value-to-pay
  (fn [db _] (:value-to-pay db)))

(re-frame/reg-sub
  ::payment-request-ok
  (fn [db _] (:payment-request-ok db)))

(re-frame/reg-sub
  ::error-processing-payment
  (fn [db _] (:error-processing-payment db)))

(re-frame/reg-sub
  ::last-sent-payment
  (fn [db _] (:last-sent-payment db)))

(re-frame/reg-sub
  ::add-contact-loading?
  (fn [db _] (:add-contact-loading? db)))

(re-frame/reg-sub
  ::send-payment-loading?
  (fn [db _] (:send-payment-loading? db)))

(re-frame/reg-sub
  ::claim-payment-loading?
  (fn [db _] (:claim-payment-loading? db)))
