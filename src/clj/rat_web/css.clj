(ns rat-web.css
  (:require [garden.def :refer [defstyles]]))

(defstyles screen
  [:body {:color "black"}]
  [:.container.main {:margin-top "1rem"}]
  [:.negative {:color "#db2828"}]
  [:.positive {:color "#21ba45"}])
